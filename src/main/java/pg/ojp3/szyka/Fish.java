package pg.ojp3.szyka;

/**
 * Created by Szymon on 18.01.2017.
 */
public class Fish extends Animal {
    public Fish() {
    }

    public Fish(String name, double weight) {
        super(name, weight);
    }

    @Override
    public void clean() {
        System.out.println(name+"'s aquarium is being cleaned.");
    }

    @Override
    public String toString() {
        return  super.toString();
    }
}
