package pg.ojp3.szyka;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by student on 2017-01-19.
 */
public class Cataloque {
    Map<Animal,Integer> catal = new HashMap<Animal,Integer>();

    public Cataloque() {
    }

    void add(Animal a,Integer i){
        catal.put(a,i);
    }
    void remove(Animal a){
        catal.remove(a);

    }

    void showCataloque(){
       catal.forEach((k,v) -> System.out.println("Animal: "+k+" Price:"+v));

    }

}
