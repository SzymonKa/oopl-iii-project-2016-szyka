package pg.ojp3.szyka;

/**
 * Created by Szymon on 18.01.2017.
 */
public class Animal  implements Feedable,Cleanable{
    protected String name;
    protected double weight;

    public Animal() {
    }

    public Animal(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) throws ImposibleWeightException {
        if (weight<=0){
            throw new ImposibleWeightException();
        }else{
            this.weight = weight;
        }
    }

    @Override
    public void clean() {}

    @Override
    public void feed() {
        System.out.println(name + " is being fed.");
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}