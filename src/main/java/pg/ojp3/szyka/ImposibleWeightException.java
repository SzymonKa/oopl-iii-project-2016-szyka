package pg.ojp3.szyka;

/**
 * Created by Szymon on 18.01.2017.
 */
public class ImposibleWeightException extends Exception {
    @Override
    public String getMessage() {
        return "Inserted weight is imposible.";
    }
}

