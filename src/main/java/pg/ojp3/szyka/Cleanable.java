package pg.ojp3.szyka;

/**
 * Created by Szymon on 18.01.2017.
 */
public interface Cleanable {
    void clean();
}
