package pg.ojp3.szyka;

/**
 * Created by Szymon on 18.01.2017.
 */
public class Parakeet extends Bird{
    public Parakeet() {
    }

    public Parakeet(String name, double weight) {
        super(name, weight);
    }

    @Override
    public String toString() {
        return "Parakeet{ " + super.toString();
    }
}
