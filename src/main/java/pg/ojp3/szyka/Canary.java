package pg.ojp3.szyka;

/**
 * Created by Szymon on 18.01.2017.
 */
public class Canary extends Bird {
    public Canary() {
    }

    public Canary(String name, double weight) {
        super(name, weight);
    }

    @Override
    public String toString() {
        return "Canary{ " + super.toString();
    }
}
