package pg.ojp3.szyka;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Szymon on 18.01.2017.
 */
public class PetShop {
    public static void main(String[] args) {
        Animal goldfish1 =new Goldfish("Anthony",0.7);
        Animal goldfish2 =new Goldfish("Anna",0.5);
        Animal parakeet1 =new Parakeet("Kiki",3.2);

        List<Animal> animals =new ArrayList<Animal>();

        Cataloque cataloque =new Cataloque();

        animals.add(goldfish1);
        animals.add(goldfish2);
        animals.add(parakeet1);

        cataloque.add(goldfish1,115);
        cataloque.add(goldfish2,124);
        cataloque.add(parakeet1,620);

        for (Animal x: animals) {
            x.feed();
            x.clean();
        }

        cataloque.showCataloque();

    }
}

