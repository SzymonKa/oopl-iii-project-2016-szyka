package pg.ojp3.szyka;

/**
 * Created by Szymon on 18.01.2017.
 */
public class Bird extends Animal {

    public Bird() {
    }

    public Bird(String name, double weight) {
        super(name, weight);
    }

    @Override
    public void clean() {
        System.out.println(name+" is takin shower");
    }

    @Override
    public String toString() {
        return  super.toString();
    }
}
