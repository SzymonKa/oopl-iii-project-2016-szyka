package pg.ojp3.szyka;

/**
 * Created by Szymon on 18.01.2017.
 */
public class Goldfish extends Fish {
    public Goldfish() {
    }

    public Goldfish(String name, double weight) {
        super(name, weight);
    }

    @Override
    public String toString() {
        return "Goldfish{ " + super.toString();
    }
}


